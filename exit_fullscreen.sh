#!/bin/bash
# From https://code.google.com/p/chromium/issues/detail?id=382126
ACTIVE=$(xdotool getactivewindow)
if [ -z "$ACTIVE" ]; then
  exit
fi

FULLSCREEN_STATE=$(xprop -id $ACTIVE _NET_WM_STATE | grep "_NET_WM_STATE_FULLSCREEN")
if [ -z "$FULLSCREEN_STATE" ]; then
  exit
fi

wmctrl -i -r $ACTIVE -b remove,fullscreen
wmctrl -i -r $ACTIVE -b remove,maximized_horz,maximized_vert
xdotool windowsize $ACTIVE 600 600
