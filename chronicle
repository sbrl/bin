#!/usr/bin/env bash


##########################
### Colour Definitions ###
#### ANSI color codes ####
RS="\033[0m"    # reset
HC="\033[1m"    # hicolor
UL="\033[4m"    # underline
INV="\033[7m"   # inverse background and foreground
LC="\033[2m"    # locolor / dim
FBLK="\033[30m" # foreground black
FRED="\033[31m" # foreground red
FGRN="\033[32m" # foreground green
FYEL="\033[33m" # foreground yellow
FBLE="\033[34m" # foreground blue
FMAG="\033[35m" # foreground magenta
FCYN="\033[36m" # foreground cyan
FWHT="\033[37m" # foreground white
BBLK="\033[40m" # background black
BRED="\033[41m" # background red
BGRN="\033[42m" # background green
BYEL="\033[43m" # background yellow
BBLE="\033[44m" # background blue
BMAG="\033[45m" # background magenta
BCYN="\033[46m" # background cyan
BWHT="\033[47m" # background white


CSECTION=${HC}${FBLE};
CTOKEN=${FCYN};
CACTION=${FYEL};
##########################

CHRONICLE_DIR="${CHRONICLE_DIR:-/home/${USER}/Nextcloud/Documents/c}";
if [ "$CHRONICLE_DIR" = "" ]; then
	CHRONICLE_DIR="/keybase/private/${USER}/chronicle";
fi


if [[ "$#" -lt 1 ]]; then
	echo -e "chronicle";
	echo -e "    By Starbeamrainbowlabs";
	echo -e "${LC}A journaling helper script.${RS}";
	echo -e "";
	echo -e "${CSECTION}Usage${RS}";
	echo -e "    chronicle {action}";
	echo -e "";
	echo -e "${CSECTION}Actions${RS}";
	echo -e "    ${CACTION}today${RS}        Write a new entry for today";
	echo -e "";
	echo -e "${CSECTION}Environment Variables${RS}";
	echo -e "    ${CACTION}CHRONICLE_DIR${RS}   The directory in which to store entries. Default: ${CTOKEN}/home/${USER}/Nextcloud/Documents/.c${RS}";
fi

edit-file() {
	which sensible-editor >/dev/null 2>&1;
	if [[ "$?" -eq 0 ]]; then
		sensible-editor $1;
	elif [[ ! -z "${EDITOR}" ]]; then
		$EDITOR $1;
	else
		echo -e "${HC}${FRED}Error: Unable to start editor (sensible-editor not found & EDITOR environment variable not found)" >&2;
		exit 1;
	fi
}

case $1 in
	today)
		temp_filename=$(mktemp --suffix "-chronicle-entry.md");
		target_filename=${CHRONICLE_DIR}/$(date +"%Y/%Y-%m.md");
		echo "# Enter the new chronicle entry here (empty lines or lines beginning with a # are ignored)." >${temp_filename};
		
		edit-file ${temp_filename};
		
		mkdir -p $(dirname ${target_filename});
		# awk: Strip empty lines and lines beginning with #, and prefix with the date
		cat ${temp_filename} | awk -v date=$(date +"%Y-%m-%d") 'BEGIN { printf(date ": "); } /^[^#]/ { print $0 } END { printf("\n") }' >>${target_filename};
		
		echo -e "Entry saved to ${CTOKEN}${target_filename}${RS}";
		
		rm ${temp_filename};
		;;
esac
