#!/usr/bin/env bash
increment=10
backlight_prefix=/sys/class/backlight/intel_backlight/

echo $1
if [[ "$1" = "decrease" ]]; then
	echo decreasing
	increment=$(expr -${increment})
fi

cur_brightness=$(cat ${backlight_prefix}brightness)
max_brightness=$(cat ${backlight_prefix}max_brightness)

new_brightness=$(expr ${cur_brightness} + ${increment})

# Permissions changes on brightness: 
## change group to sbrl
## add g+w
# Old command:
#gksudo -- bash -c "echo ${new_brightness} >${backlight_prefix}brightness"
echo ${new_brightness} >${backlight_prefix}brightness

####################
### Notification ###
####################
### uncomment the following line to disable the notification
#exit
# Calculate the percentage
new_percent=$(echo "(${new_brightness} / ${max_brightness}) * 100" | bc -l)
new_percent=$(printf "%.1f" "${new_percent}")

echo new_percent: $new_percent


max_bar_length=100
bar_length=$(echo "(${new_percent} / 100) * ${max_bar_length}" | bc -l)
bar_length=$(printf "%.0f" "${bar_length}")

n_bar=$(head -c $bar_length < /dev/zero | tr '\0' '=')

# Kill the previous notification
killall notify-osd
notify-send "Brightness: ${new_percent}%" "${n_bar} (${new_brightness})"
#notify-send "Brightness" "${new_percent}%"

