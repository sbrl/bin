#!/usr/bin/env bash
###############
### Aliases ###
###############

# cd up multiple directories at a time
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias ......='cd ../../../../../'
alias .......='cd ../../../../../../'
alias ........='cd ../../../../../../../'
alias .........='cd ../../../../../../../../'
alias ..........='cd ../../../../../../../../../'

# Some more ls aliases
alias sl='ls';

if ls /dev/shm --hyperlink=auto >/dev/null 2>&1; then
	alias ll='ls -hAtFl --hyperlink=auto';
	alias la='ls -hA --hyperlink=auto';
	alias l='ls -htFl --hyperlink=auto';
else
	alias ll='ls -hAtFl';
	alias la='ls -hA';
	alias l='ls -htFl';
fi

if ! which tree >/dev/null 2>&1; then
	alias tree='find . | sed -e "s/[^-][^\/]*\//  |/g" -e "s/|\([^ ]\)/|-\1/"';
fi

alias asciibanner="toilet -f regular";
alias toilet="toilet -f regular";

# less - display line numbers, and allow colour
alias less='less -R'

# Make cp and mv prompt before overwriting
alias cp='cp -i'
alias mv='mv -i'


# Make everything tell us what they're doing
alias mkdir='mkdir -pv'
alias rm='rm -v'
alias rmdir='rmdir -v'


# Make the permissions tweaking commands give us more information about what they are doing
alias chmod='chmod -c'
alias chown='chown -c'
alias chgrp='chgrp -c'

# Quiet, ffmpeg!
alias ffmpeg='ffmpeg -hide_banner'
alias ffprobe='ffprobe -hide_banner'

# Convert flac files to mp3
flac2mp3() {
	dir="${1}";
	if [[ -z "${dir}" ]]; then dir="."; fi
	#shellcheck disable=SC2016
	find "${dir}" -iname '*.flac' -type f -print0 | nice -n20 xargs -P "$(nproc)" --null --verbose -n1 -I{} sh -c 'old="{}"; new="${old%.*}.mp3"; ffmpeg -i "${old}" -ab 320k -map_metadata 0 -c:v copy -disposition:v:0 attached_pic -id3v2_version 3 "${new}";';
}

mp3cover() {
	cover="${1}";
	dir="${2}";
	
	if [[ -z "${cover}" ]] || [[ -z "${dir}" ]]; then
		echo "Usage:" >&2;
		echo "    mp3cover path/to/cover_image.jpg path/to/album_dir";
		return 0;
	fi
	
	find "${dir}" -type f -iname '*.mp3' -print0 | xargs -0 -P "$(nproc)" eyeD3 --add-image "${cover}:FRONT_COVER:"
}

repl() {
	preview="${1}"; # MAKE SURE THIS HAS the substring {q} in it for where you want the thing to go!
	echo '' | fzf --print-query --preview "${preview}";
}

# Compress PDFs
pdfcompress() {
	local filename="${1}";
	
	if [[ -z "${filename}" ]]; then
		echo "Usage:
	pdfcompress <filename.pdf>

...where <filename.pdf> is the pdf to compress. Will be replaced with the compressed copy, except if the compressed version is larger than the original.";
		return 0;
	fi
	
	local filesize_before
	filesize_before="$(wc -c <"${filename}")";
	
	ps2pdf -dPDFSETTINGS=/prepress "${filename}" "${filename}.temp.pdf";
	
	local filesize_after;
	filesize_after="$(wc -c <"${filename}.temp.pdf")";
	local difference="$((filesize_before-filesize_after))";
	
	echo ">>> BEFORE $(echo "$filesize_before" | numfmt --to=iec)iB";
	echo ">>> AFTER  $(echo "$filesize_after" | numfmt --to=iec)iB";
	
	if [[ "${filesize_after}" -lt "${filesize_before}" ]]; then
		echo ">>> Compressed by $(echo "${difference}" | numfmt --to=iec)iB";
		mv -f "${filename}.temp.pdf" "${filename}";
	else
		echo ">>> Compressed file was $(echo "$((-difference))" | numfmt --to=iec)iB larger, discarding";
		rm "${filename}.temp.pdf";
	fi
}

# Make time work properly, but only if the local binary is present
if [[ -f /usr/bin/time ]]; then
	alias time='/usr/bin/time';
fi

# dfh: Show disk usage in a form we can actually understand
alias dfh='df -hT'

# lsblkl: more lsblk info
alias lsblkl='lsblk -o NAME,RO,SIZE,FSTYPE,RM,TYPE,MOUNTPOINT,LABEL,VENDOR,MODEL'

# du: show last line only
alias duh='du -hl | tail -n1'

# zcat: gunzip & display compressed gz files.
if ! which zcat >/dev/null 2>&1; then
	alias zcat='gunzip -c'
fi

# Alias to clear the screen for real
alias cls='printf "\033c"'

# mkfifo -> mkpipe
alias mkpipe='mkfifo'

# OpenRC helpers
if command -v rc-service >/dev/null 2>&1; then
	alias service='rc-service';
fi
if command -v rc-update >/dev/null 2>&1; then
	alias update='rc-update';
fi
if command -v rc-status >/dev/null 2>&1; then
	alias status='rc-status';
fi

# fdfind is the apt package binary for https://crates.io/crates/fd-find, but  fdfind is inconvenient when fd is nominal
if command -v fdfind >/dev/null 2>&1; then
	alias fd=fdfind;
fi

# Get scrollback back - ref https://github.com/mobile-shell/mosh/issues/122#issuecomment-623792008
alias mosh='mosh --no-init';

# Use vimen to create new files.
alias vimen="vim -u \"\${HOME}/.config/.vimencrypt\" -x";
alias vime="vim -u \"\${HOME}/.config/.vimencrypt\"";

timer() {
	i="${1}";
	if [[ -z "${i}" ]]; then
		echo "Usage:
	timer {{SECONDS}}

...where {{SECONDS}} is a positive integer.";
	fi
	exec {sleep_fd}<> <(:) # Ref https://github.com/dylanaraps/pure-bash-bible?tab=readme-ov-file#use-read-as-an-alternative-to-the-sleep-command
	while [[ "${i}" -gt 0 ]]; do
		i="$((i - 1))";
		echo -ne "${i}        \r";
		read -rt 1 -u "${sleep_fd}";
	done
}
stopwatch() {
	clock="0";
	subclock="0";
	exec {sleep_fd}<> <(:) # Ref https://github.com/dylanaraps/pure-bash-bible?tab=readme-ov-file#use-read-as-an-alternative-to-the-sleep-command
	while :; do
		subclock="$((subclock + 1))";
		if [[ "${subclock}" -gt 9 ]]; then
			clock="$((clock + 1))";
			subclock="0";
		fi
		echo -ne "\r${clock}.${subclock}";
		read -rt 0.1 -u "${sleep_fd}";
	done
}

# allow us to create a directory and immediately move into it
mcd() { mkdir -pv "$1" && { cd "$1" || { echo "Error: Failed to cd to $1"; return; } };}

# Set the window title
set-title() {
	#shellcheck disable=SC1003
	echo -e '\033]2;'"$1"'\033\\';
	# Tell set_bash_prompt that we don't want it to reset the terminal title again
	#shellcheck disable=SC2034
	TITLE_SET_MANUALLY=true;
}
alias t="set-title"

# Display a sorted list of processes and their swap usage 
# Source: https://www.cyberciti.biz/faq/linux-which-process-is-using-swap/
swaptop() {
	for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " " $3}END{ print ""}' "${file}"; done | sort -k 2 -n -r | less
}

alias digs="dig +short"

# Text Processing
# From https://www.reddit.com/r/tinycode/comments/c63gr6/deblank_remove_blank_lines_from_a_file_or_stdin/
alias deblank='grep -vE "^\s*$"'

# hr
alias hrh='hr - - = - -'

# termbins
if which ncat >/dev/null 2>&1; then
	alias sbrlbin='ncat starbeamrainbowlabs.com 9999';
	alias termbin='ncat termbin.com 9999';
elif which netcat >/dev/null 2>&1; then
	alias sbrlbin='netcat starbeamrainbowlabs.com 9999';
	alias termbin='netcat termbin.com 9999';
elif which nc >/dev/null 2>&1; then
	alias sbrlbin='nc starbeamrainbowlabs.com 9999';
	alias termbin='nc termbin.com 9999';
else
	alias sbrlbin='echo "Failed to find ncat, netcat, or nc :-("';
	alias termbin='echo "Failed to find ncat, netcat, or nc :-("';
fi

# termbin for any kind of file
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi 
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> "${tmpfile}"; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> "${tmpfile}" ; fi; cat "${tmpfile}"; command rm -f "${tmpfile}"; }

# gitignore.io interface
gitignore() { curl -L -s "https://www.gitignore.io/api/$*"; }

# short git aliases, ref <https://github.com/algotech/dotaliases/blob/master/doc/bash/git_aliases.md>
alias g="git status -sb"
alias ga="git add"
alias gr="git rm"
alias gd="git diff"
alias gchk="git checkout -- "
alias gc="git commit"
alias gac="git add --all && git commit"
alias gca="git commit -a"
alias gcm="git commit -m"
alias gacm="git add --all && git commit -m"
alias gp="git pull"
alias gf="git fetch"
alias gpu="git push"
alias glp="git log --online --graph --decorate"
alias gl="git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"


# git v2.15+
#if [[ "$(git --version | awk 'BEGIN { FS="." } { print $2 }')" -ge 15 ]]; #then
#	alias 'git-diff'='git diff --color-moved'
#	alias 'git-show'='git show --color-moved'
#fi

alias ggs="git status";

alias bf="btrfs filesystem"
alias bfss="btrfs filesystem show" # bts = breath-first search alternative to findsudo btrfs filesystem usage /mnt/elfstone2
alias bfu="btrfs filesystem usage"
alias bb="btrfs balance"
alias bs="btrfs subvolume"
alias bsl="btrfs subvolume list"
alias bsc="btrfs subvolume create"
alias bsd="btrfs subvolume delete"

# Get / Set clipboard contents
alias setclip='xclip -selection c'
alias getclip='xclip -selection clipboard -o'

# Ubuntu's inbuilt image previewer. I'll never be able to remember eog...
alias image-preview=eog
alias vn=viewnior


if command -v netscanner >/dev/null 2>&1; then
	alias netscanner='sudo "$(which netscanner)"';
fi

if command -v dust >/dev/null 2>&1; then alias dustd="dust -D"; fi

# URL Encode / Decode
# From https://unix.stackexchange.com/a/216318/64687
alias urlencode='python -c "import urllib, sys; print urllib.quote(sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1], \"\")"'
alias urldecode='python -c "import urllib, sys; print urllib.unquote(sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1])"'

va() { # Tool to activate virtual environments easily
	venv_root="${HOME}/Documents/venvs";
	echo ">>> venv root: ${venv_root}";
	if [[ ! -d "${venv_root}" ]]; then
		echo "Warning: venvs directory at ${venv_root} doesn't exist, creating" >&2;
		return 1;
	fi
	venv="${1}";
	if [[ ! -d "${venv_root}/${venv}" ]]; then
		echo "Error: No venv with name ${venv} could be found.";
		return 1;
	fi
	#shellcheck disable=SC1090 source=/dev/null
	source "${venv_root}/${venv}/bin/activate";
	echo ">>> Activated venv ${venv}";
}

# Activate coloured output in ncdu
alias ncdu='ncdu --color dark'

# Single-level recursive git
rgit() {
	# TODO: Make this work in parallel
	for dirname in $(find . -maxdepth 2 -type d -name .git); do
		pushd "$(dirname "$dirname")" >/dev/null || { echo "Failed to pushd to ${dirname}"; return 1; };
		echo "Repository: ${PWD}"
		git "$@";
		popd >/dev/null || { echo "Failed to popd from ${dirname}"; return 1; };
	done
}
rgit-update() {
	# BUG: pushd needs to be quiet, and git log is still opening less
	for dirname in $(find . -maxdepth 2 -type d -name .git); do
		pushd "$(dirname "$dirname")" || { echo "Failed to pushd $dirname"; return 1; };
		pwd
		git fetch;
		
		git log --oneline --graph master..origin/master | cat;
		[[ "$(git log --oneline --graph master..origin/master | wc -l)" -gt 0 ]] && read -p "Merge Changes? (Enter) - (^C to abort)";
		
		git merge;
		
		popd || { echo "Failed to popd."; return 1; };
	done
}

# colourise man
if command -v bat >/dev/null 2>&1; then
	export MANPAGER="sh -c 'sed -u -e \"s/\\x1B\[[0-9;]*m//g; s/.\\x08//g\" | bat -p -lman'";
else
	_colorman() {
	env \
		LESS_TERMCAP_mb="$(printf "\e[1;35m")" \
		LESS_TERMCAP_md="$(printf "\e[1;34m")" \
		LESS_TERMCAP_me="$(printf "\e[0m")" \
		LESS_TERMCAP_se="$(printf "\e[0m")" \
		LESS_TERMCAP_so="$(printf "\e[7;40m")" \
		LESS_TERMCAP_ue="$(printf "\e[0m")" \
		LESS_TERMCAP_us="$(printf "\e[1;33m")" \
		"$@"
	}
	man() { _colorman man "$@"; }
fi

# Quickly jump around places
telepeek() {
	find . -type d 2>/dev/null | grep -iP "$@" | cat -n | sed 's/^[ 0-9]*[0-9]/\o033[34m&\o033[0m/' | less -R
}
teleport() {
	cd "$(find . -type d 2>/dev/null | grep -m1 -iP "$@")" || { echo "Failed to cd into directory" >&2; return 1; };
}
telepick() {
	telepeek "$@";
	read -r -p "jump to index: " line_number;
	cd "$(find . -type d 2>/dev/null | grep -iP "$1" | sed "${line_number}q;d")" || { echo "Failed to cd into directory" >&2; return 1; };
}

# Archivebox remote
# TODO: Make this work remotely by going via starbeamrainbowlabs.com if needed
__archive-url() {
	url="$1";
	if [[ "${url}" == "" ]]; then read -r -p "Url to archive: " url; fi
	ssh elessar -t "cd /srv/ArchiveBox; ./archive-url \"${url}\"";
}
alias archive-url=__archive-url;

# Download a Youtube video, convert to audio, and grab the thumbnail
if command -v yt-dlp >/dev/null 2>&1; then
	alias ytat="yt-dlp --output '%(title)s.%(ext)s' -x --audio-format mp3 --write-thumbnail";
else
	alias ytat="youtube-dl --output '%(title)s.%(ext)s' -x --audio-format mp3 --write-thumbnail"; # youtube-dl as fallback
fi

# Easy checking sha256 checksum files
alias shav="sha256sum --ignore-missing -c"

# Quick slurm aliases
alias squeueu='squeue -u "${USER}"';
alias squeuea='squeue --all --long';

# Connect to the University Palo Alto / Global Protect VPN
# CAUTION: If you use this, it's at your own risk
univpn() {
	set-title openconnect
	sudo openconnect --protocol gp pa-vpn.hull.ac.uk --usergroup gateway --csd-wrapper "${HOME}/bin/scripts/hipreport.sh" --local-hostname="$(echo "$HOSTNAME" | sha256sum | cut -c 1-12)" --os=linux-64;
}

alias zo="zoxide";
