# ~/.bash_profile: executed by bash(1) for login shells.
sbrl_bashprofile_executed=true;

if [ "${sbrl_bashrc_executed}" = "" ]; then
	source $HOME/.bashrc;
fi

if [ "${BASH_PROFILE_BANNER}" != "no" ] && [ ! -f "/dev/shm/cloudcatcher-banner.txt" ]; then
	$HOME/bin/cloudcatcher/cloudcatcher.sh >/dev/shm/cloudcatcher-banner.txt;
fi

if [ "${BASH_PROFILE_BANNER}" != "no" ]; then
	cat /dev/shm/cloudcatcher-banner.txt;
fi
