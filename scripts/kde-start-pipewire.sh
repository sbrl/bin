#!/usr/bin/env sh

pipewire & disown
pipewire-pulse & disown
wireplumber & disown

exit 0
