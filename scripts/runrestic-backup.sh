#!/usr/bin/env bash
# TODO: Blog about this


# $1 - Command name to check for
# $2 - Optional. If set to "optional", then a failure will not terminate the build process.
check_command() {
	echo -ne ">>> Checking for $1:";
	which "$1" >/dev/null 2>&1; exit_code="$?";
	if [[ "${exit_code}" -ne 0 ]]; then
		echo -e "failed!";
		echo -e "Error: Couldn't locate $1. Make sure it's installed and in your path.";
		if [ "$2" != "optional" ]; then
			exit 2;
		fi
	else
		echo -e "complete";
	fi
}

check_command "runrestic";
check_command "yad";

yad --center --width 540 --height 180 --text-align center --image "dialog-question" --title "Run backup?" --button=gtk-yes:0  --button=gtk-no:1 --text "Run userfiles runrestic?";
exit_code="$?";

if [[ "$exit_code" -eq 0 ]]; then
	echo ">>> Running Restic" >&2;
	runrestic --show-progress 1;
	# sleep 1;
	# echo "(for testing runrestic wasn't actually run)";

	echo ">>> Complete, exit code $?";
	sleep 3;
else
	echo ">>> Not running restic.";
	sleep 3;
fi
