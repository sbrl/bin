#!/usr/bin/env bash

echo "[Thumbnailer Entry]
Version=1.0
Encoding=UTF-8
Type=X-Thumbnailer
Name=Folder Thumbnailer
MimeType=inode/directory;
Exec=sh -c '~/bin/scripts/thunar-folder-thumbnailer.sh \"$@\"' _ %s %i %o %u" >"${HOME}/.local/share/thumbnailers/folder.thumbnailer";
echo ">>> Written custom folder thumbnailer to ${HOME}/.local/share/thumbnailers/folder.thumbnailer";
