#!/bin/bash

# Sound to play when the battery is low (set to an empty string or comment out to disable)
notification_sound="/usr/share/sounds/ubuntu/notifications/Rhodes.ogg";

# Notification text to show
notification_primary="WARNING: Battery is about to die";
notification_secondary="Plug in the power cable";

notification_percentage="15";   # Show a notification at this percentage
notification_interval="20";     # Sleep for this long after showing a notification

check_interval="10";            # Check every this many seconds

BATTERY="$(upower -e | grep 'BAT')";

snore() {
    # Usage: snore 1
    #        snore 0.2
    read -rt "$1" <> <(:) || :
}

while :
do
    BATTERY_PERCENTAGE=$(upower -i $BATTERY|awk '/percentage/ { gsub("%", "", $2); print $2 }')
    CABLE=$(upower -d | awk '/line-power/ { start=1 } /online/ { if (start=1) { print $2; exit } }');

    if [[ "${BATTERY_PERCENTAGE}" -lt "${notification_percentage}" && "${CABLE}" = "no" ]]; then
        notify-send --icon=battery-empty --urgency=critical "${notification_primary}"; "${notification_secondary}";
        [[ ! -z "${notification_sound}" ]] && play --no-show-progress "${notification_sound}" >/dev/null 2>&1;
        
        snore "${notification_interval}";
    fi
    
    snore "${check_interval}";
done
