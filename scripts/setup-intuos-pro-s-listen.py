#!/usr/bin/env python3
# Ref https://gist.github.com/altbrace/52ae1783b31257021520673fadb95b6e

from pydbus import SystemBus
from gi.repository import GLib  # don't mind the import error if you get one, it should work
import subprocess
import time
import re

ADDRESS = 'E0_9F_2A_00_71_8F'  # your Bluetooth device's MAC separated by underscores


def pc_handler(sender=None, iface=None, signal=None, object=None, arg0=None):
    dev_api = dev['org.bluez.Device1']
    if dev_api.Connected:
        print(f"Device {ADDRESS} connected, sleeping...")
        time.sleep(2)  # the system needs some time before the profile can be changed. You can experiment with this value.

        subprocess.call(['bash', '/home/sbrl/bin/scripts/setup-intuos-pro-s.sh'])  # profile change command
            # You can call whichever command you wish, just change the subprocess call accordingly

    else:
        print(f"Device {ADDRESS} disconnected.")


bus = SystemBus()

bluez_desc = bus.get('org.bluez').Introspect()  # get bluez object description, there are defined connected adapters
ifaces = re.findall("node name=\"(hci.)\"", bluez_desc)  # get adapter ids
if not ifaces:
    raise Exception("No bluetooth interfaces available, check your adapter")

dev = ""
dev_iface = ""
print(f"Running lookup for {ADDRESS}...")
for iface in ifaces:
    print(f"Checking {iface}...")  # checking each available bluez interface for our device present
    try:
        dev = bus.get('org.bluez', f'/org/bluez/{iface}/dev_{ADDRESS}')   # get the device by dbus path
        print(f"Device {ADDRESS} was found at {iface}.")
        dev_iface = iface
        break
    except KeyError:
        print(f"Device {ADDRESS} was not found at {iface}, continuing.")

if not dev:
    raise Exception(f"Device {ADDRESS} was not found in the system, please make sure to pair it first")

listener = bus.subscribe(iface='org.freedesktop.DBus.Properties', signal='PropertiesChanged',
                         object=f'/org/bluez/{dev_iface}/dev_{ADDRESS}',
                         arg0='org.bluez.Device1', signal_fired=pc_handler)

loop = GLib.MainLoop()
loop.run()