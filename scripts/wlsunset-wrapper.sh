#!/usr/bin/env bash


# Kill any existing wlsunset instances
existing="$(pidof wlsunset)";

if [[ -n "${existing}" ]]; then
    #shellcheck disable=SC2086
    kill ${existing};
fi

# Grab our current location
user_agent="bash/${BASH_VERSION} curl/$(pacman -Qi curl | grep -Po '^Version\s*: \K.+') $(jq --version | tr '-' '/') (Wayland; $(uname -srm)) ${XDG_CURRENT_DESKTOP}/$(pacman -Qi plasma-desktop | grep -Po '^Version\s*: \K.+')";
read -r lat lng < <(curl -H "user-agent: ${user_agent}" -sSL ipinfo.io | jq --raw-output '.loc' | tr ',' ' ');

wlsunset  -l "${lat}" -L "${lng}" & disown
