#!/usr/bin/env bash

if [[ -z "$DISPLAY" ]]; then
	# If not set xsetwacom naturally refuses to do anything
	export DISPLAY=":0";
fi

#              ██     ██  █████   ██████  ██████  ███    ███                  
#              ██     ██ ██   ██ ██      ██    ██ ████  ████                  
#              ██  █  ██ ███████ ██      ██    ██ ██ ████ ██                  
#              ██ ███ ██ ██   ██ ██      ██    ██ ██  ██  ██                  
#               ███ ███  ██   ██  ██████  ██████  ██      ██                  
                                                                            
# ██ ███    ██ ████████ ██    ██  ██████  ███████     ██████  ██████   ██████ 
# ██ ████   ██    ██    ██    ██ ██    ██ ██          ██   ██ ██   ██ ██    ██
# ██ ██ ██  ██    ██    ██    ██ ██    ██ ███████     ██████  ██████  ██    ██
# ██ ██  ██ ██    ██    ██    ██ ██    ██      ██     ██      ██   ██ ██    ██
# ██ ██   ████    ██     ██████   ██████  ███████     ██      ██   ██  ██████ 

###
# RIGHT HANDED
###
                                                                    
#    ┌──────────────────────────────────────────────────────────────┐        
#    │                                                              │        
#    │ ┌───┐      ┌──                                        ──┐    │        
#    │ │ 1 │      │                                            │    │        
#    │ ├───┤                                                        │        
#    │ │|2|│                                                        │        
#    │ ├───┤                                                        │        
#    │ │ 3 │                                                        │        
#    ├─┴───┴───┐                                                    │        
#    │         │                                                    │        
#    │ ┌────┐  │                       PAD                          │        
#    │ │ 11 │  │                                                    │        
#    │ └────┘  │                                                    │        
#    │         │                                                    │        
#    ├─┬───┬───┘                                                    │        
#    │ │ 8 │                                                        │        
#    │ ├───┤                                                        │        
#    │ │|9|│                                                        │        
#    │ ├───┤                                                        │        
#    │ │ 10│       │                                           │    │        
#    │ └───┘       └──                                       ──┘    │        
#    │                                                              │        
#    └──────────────────────────────────────────────────────────────┘        

###
# Left Handed
###
#    ┌──────────────────────────────────────────────────────────────┐
#    │                                                              │
#    │  ┌──                                        ──┐      ┌───┐   │
#    │  │                                            │      │ 10│   │
#    │                                                      ├───┤   │
#    │                                                      │|9|│   │
#    │                                                      ├───┤   │
#    │                                                      │ 8 │   │
#    │                                                   ┌──┴───┴───┤
#    │                                                   │          │
#    │                       PAD                         │  ┌────┐  │
#    │                                                   │  │ 11 │  │
#    │                                                   │  └────┘  │
#    │                                                   │          │
#    │                                                   └──┬───┬───┤
#    │                                                      │ 3 │   │
#    │                                                      ├───┤   │
#    │                                                      │|2|│   │
#    │                                                      ├───┤   │
#    │   │                                           │      │ 1 │   │
#    │   └──                                       ──┘      └───┘   │
#    │                                                              │
#    └──────────────────────────────────────────────────────────────┘

# Ref https://github.com/linuxmint/cinnamon-control-center/issues/212#issuecomment-681278552

# udev rule:
# -----------
# ACTION=="add", SUBSYSTEM=="hid", ENV{HID_NAME}=="IntuosPro S", RUN+="/usr/bin/env bash /home/sbrl/bin/scripts/setup-intuos-pro-s.sh"
# -----------
# Add this to a new file in /etc/udev/rules.d
# NOTE: udev rule doesn't work 'cause desktop environment context is required :-/


DEVICE="Wacom Intuos Pro S"
STYLUS="Wacom Intuos Pro S Pen stylus"
ERASER="Wacom Intuos Pro S Pen eraser"
PAD="Wacom Intuos Pro S Pad pad"
CURSOR="Wacom Intuos Pro S Pen cursor"


###
# Pad
###

# Buttons
xsetwacom --set "$PAD" Button 8 "key ctrl shift z"
xsetwacom --set "$PAD" Button 9 "key ctrl s"
xsetwacom --set "$PAD" Button 10 "key ctrl z"

xsetwacom --set "$PAD" Button 11 "key b" # Krita: brush

xsetwacom --set "$PAD" Button 3 "key ctrl t" # Krita: transform → raindrop tool
xsetwacom --set "$PAD" Button 2 "key ctrl" # Krita: eyedropper
xsetwacom --set "$PAD" Button 1 "key ctrl r" # Krita: select tool


# Wheel: zoom in/out
xsetwacom set "$PAD" "AbsWheelUp" "key ctrl shift plus"
xsetwacom set "$PAD" "RelWheelUp" "key ctrl shift plus"
xsetwacom set "$PAD" "RelWheelDown" "key ctrl minus"
xsetwacom set "$PAD" "RelWheelDown" "key ctrl -"
xsetwacom set "$PAD" "AbsWheelDown" "key ctrl -"

###
# Stylus
###

# Left handed mode
xsetwacom set "$STYLUS" "Rotate" "half"

# Default is max pressure is like really really hard, so this fixes it
xsetwacom set "$STYLUS" PressureCurve 0 30 70 100


notify-send --icon input-tablet --urgency critical "Intuos Pro S connected"  "Preconfigured settings applied"
