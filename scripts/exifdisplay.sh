#!/usr/bin/env bash

temp_dir="$(mktemp --tmpdir -d "exifcomment-XXXXXXX")";

on_exit() {
	rm -rf "${temp_dir}";
}
trap on_exit EXIT;


command_exists() {
	which "$1" >/dev/null 2>&1;
	return "$?";
}

start_text_editor() {
	target="$1";
	
	if command_exists gedit; then
		gedit "$target"; # TODO: Make this not return until finished!
	elif command_exists mousepad; then
		mousepad --disable-server "$target";
	else
		yad --text "Error: No suitable editor found.";
		exit 1;
	fi
}

if ! command_exists exiftool; then
	yad --text "Error: Can't find exiftool";
	exit 1;
fi

comment_file="${temp_dir}/comment-edit.txt";

for filename in "$@"; do
	if [[ ! -f "$filename" ]]; then continue; fi
	
	exiftool "$filename" >"$comment_file";
	
	
	start_text_editor "$comment_file";
done