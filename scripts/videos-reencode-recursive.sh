#!/usr/bin/env bash

dirpath_target="${1}";

if [[ -z "${dirpath_target}" ]]; then
	echo ">>> Usage: path/to/videos-reencode-recursive.sh {{path/to/directory}}" >&2;
	exit
fi

reencode() {
	old="${1}";
	new="${old%.*}.webm";
	ffmpeg -hide_banner -i "${old}" -max_interleave_delta 0 -c:v libvpx-vp9 -c:a libopus -crf 30 -b:v 0 "${new}";
	ec="${?}";
	
	if [[ "${ec}" -eq 0 ]]; then
		old_size="$(stat -c%s "${old}")";
		new_size="$(stat -c%s "${new}")";
		
		if [[ "${new_size}" -lt "${old_size}" ]]; then
			echo ">>> SMALLER: ${old_size} > ${new_size}, remove ${old}";
			rm "${old}";
		else
			echo ">>> BIGGER: ${old_size} < ${new_size}, remove ${new}";
			rm "${new}";
		fi
	else
		echo ">>> Error: ffmpeg exited w/code ${ec}. Not checking filesize." >&2;
	fi
}

export -f reencode;

find "${dirpath_target}" -type f \( -iname '*.mp4' -o -iname '*.mov' -o -iname '*.avi' \) -print0 | nice xargs --verbose -0 -I{} bash -c 'reencode "{}"';