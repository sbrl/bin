#!/usr/bin/env bash

# $1 - Command name to check for
# $2 - Optional. If set to "optional", then a failure will not terminate the build process.
check_command() {
		echo -ne ">>> Checking for $1:";
		which "$1" >/dev/null 2>&1; exit_code="$?";
		if [[ "${exit_code}" -ne 0 ]]; then
				echo -e "failed!";
				echo -e "Error: Couldn't locate $1. Make sure it's installed and in your path.";
				notify-send --icon error "keybase-ask error" "Could not locate $1. Make sure it's installed an in your PATH.";
				if [ "$2" != "optional" ]; then
						exit 2;
				fi
		else
				echo -e "complete";
		fi
}

check_command "run_keybase";
check_command "yad";

yad --center --width 540 --height 180 --text-align center --image "dialog-question" --title "Start keybase?" --button=gtk-yes:0  --button=gtk-no:1 --text "Start keybase?";
exit_code="$?";

if [[ "$exit_code" -eq 0 ]]; then
	notify-send --icon keybase "starting keybase"
	/usr/bin/env KEYBASE_AUTOSTART=1 run_keybase &
else
	notify-send --icon error "not starting keybase"
fi
