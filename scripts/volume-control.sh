#!/usr/bin/env bash

mode="$1";


case "${mode}" in
	UP )
		pactl set-sink-volume @DEFAULT_SINK@ +5%;
		;;
	DOWN )
		pactl set-sink-volume @DEFAULT_SINK@ -5%;
		;;
	* )
		notify-send --icon error "Error: Unknown volume control action ${mode}.";
		;;
esac