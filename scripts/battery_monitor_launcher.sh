#!/bin/bash

getRunningProcesses() {
    ps -aux | grep -v grep | grep battery_monitor.sh
}

if [[ -n "$(getRunningProcesses)" ]] ; then
    exit
fi

"${HOME}/bin/scripts/battery_monitor.sh" &
