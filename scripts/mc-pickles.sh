#!/usr/bin/env bash

winid="$1";

sleep="0.05";

if [[ -z "${winid}" ]]; then
	echo "Please use xwininfo -display :0 to determine the window ID an pass it like so:";
	echo "    path/to/mc-pickles.sh {{window_id}}";
	exit 1;
fi

# Pickles = slot 6
# Bonemeal = slot 7

# 1 = left	= break
# 3 = right	= use

echo ">>> Window id ${winid}";

for i in {1..64}; do
	echo "Round $i";
	sleep "${sleep}" && xdotool click --window "${winid}" 1	# Break pickle
	sleep "${sleep}" && xdotool key --window "${winid}" 6		# Select pickles
	sleep "${sleep}" && xdotool click --window "${winid}" 3	# Place pickle
	sleep "${sleep}" && xdotool key --window "${winid}" 7		# Select bonemeal
	sleep "${sleep}" && xdotool click --window "${winid}" 3	# Bonemeal
done