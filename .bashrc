#!/usr/bin/env bash
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
	*i*) ;;
	  *) return;;
esac

#shellcheck disable=SC2034
sbrl_bashrc_executed=true;

# Bash version tester - used to turn certain features on or off depending on support
# Source: https://stackoverflow.com/a/56201734/1460422
# $1	The minimum required major version of Bash.
# $2	The minimum required minor version of Bash.
check_bash_version() {
    local major=${1:-4}
    local minor=$2
    local rc=0
    local num_re='^[0-9]+$'

    if [[ ! $major =~ $num_re ]] || [[ $minor && ! $minor =~ $num_re ]]; then
        printf '%s\n' "ERROR: version numbers should be numeric"
        return 1
    fi
    if [[ $minor ]]; then
        local bv=${BASH_VERSINFO[0]}${BASH_VERSINFO[1]}
        local vstring=$major.$minor
        local vnum=$major$minor
    else
        local bv=${BASH_VERSINFO[0]}
        local vstring=$major
        local vnum=$major
    fi
    ((bv < vnum)) && {
        printf '%s\n' "ERROR: Need Bash version $vstring or above, your version is ${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}"
        rc=1
    }
    return $rc
}

# HISTCONTORL moved to after bashhub init at bottom of file

# append to the history file, don't overwrite it
shopt -s histappend
# Allow verification of commands coming from history so they aren't blindy executed
shopt -s histverify

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
# Allow unlimited history - see https://stackoverflow.com/a/12234989/1460422
# Requires bash v4.4+ (Corrected, Viper has Bash v4.3 and it doesn't work)
if check_bash_version 4.4 >/dev/null 2>&1; then
	export HISTSIZE=-1
	export HISTFILESIZE=-1
else
	export HISTSIZE=9999999;
	export HISTFILESIZE=9999999;
fi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

if [[ "${TERM}" == "xterm-kitty" ]]; then
	export TERM="xterm-color";
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
	if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
fi

#shellcheck source=.bash_host
source ~/.bash_host

if [ "${DOUBLE_FORCE_PROMPT_COLOUR}" = "true" ]; then
	color_prompt=yes
fi

# Make sure that nawk is available

if ! which nawk >/dev/null 2>&1; then
	alias nawk='awk';
fi
# Figure out what kind of terminal we are in - nawk is faster & uses less memory
TERMINAL_TYPE=unknown
# On Windows who doesn't work
if who >/dev/null 2>&1; then
	TERMINAL_TYPE="$(who | grep -iP "^${USER}" | awk '{ gsub(/\s+|\//, " ", $0); gsub(/[0-9]+/, "", $2); print $2 }')";
	case "${TERMINAL_TYPE}" in
		"pts") # It's probably SSH
			TERMINAL_TYPE="ssh";
			;;
		"tty") # It could be a literal terminal, or a display
			TERMINAL_TYPE="terminal";
			# If there's something like (:0) on the end of the who output, then it must be a display
			if [[ "$(who | grep -ciP "^${USER}.*\(:";)" -gt 0 ]]; then
				TERMINAL_TYPE="display";
			fi
			;;
	esac
	
	# If the SSH_CONNECTION environment variable is present, then we're almost
	# certainly logged in via SSH
	if [[ -n "${SSH_CONNECTION}" ]]; then
		TERMINAL_TYPE="ssh";
	fi
	# TERMINAL_TYPE=$(who | nawk '{ match($0, /\(([^)]+)\)/, matches); if(index(matches[1], ".") > 0) print("ssh"); else if(index(matches[1], ":") > 0) print("display"); else print("terminal"); exit}'); # Remove the exit to list all terminal types
fi
# display	blue	Connected via a pseudo-terminal on a display
# ssh		purple	Connected via an ssh session
# terminal	green	Connected directly via a terminal


prompt_colon()
{
	# Changed from $(pwd) to avoid an additional subprocess from being spawned
	if [[ -w "${PWD}" ]]; then
		#shellcheck disable=SC2028
		echo "\[\033[32m\]:\[\033[0m\]"
	else
		#shellcheck disable=SC2028
		echo "\[\033[31m\]:\[\033[0m\]"
	fi
}

prompt_at()
{
	echo -ne "${RS}";
	if [ "${DISPLAY}" != "" ]; then
		echo -ne "${FBLE}";
	else
		case ${TERMINAL_TYPE} in
			*"terminal"*)
				echo -ne "${FGRN}${HC}";
				;;
			*"ssh"*)
				echo -ne "${FMAG}";
				;;
			*"display"*)
				echo -ne "${FBLE}";
				;;
			*)
				echo -ne "${FRED}${HC}";
				;;
		esac
	fi
	echo -ne "@${RS}${HOST_COLOUR}";
}

# __record_history_entry() {
# 	if [[ "${history_ext_disable}" == "true" ]]; then
# 		return 0;
# 	fi
# 	local hostname="${1}";
# 	local cmd="${2}";
# 	local exit_code="${3}";
# 	local cmd_number="${4}";
# 	local datetime="${5}";
# 	local cwd="${6}";
# 
# 	echo "${datetime}	${hostname}	${cmd_number}   ${exit_code}	${cwd}	${cmd}" >>"${HOME}/.bash_history_ext.tsv";
# }

set_bash_prompt() {
	# Update the history file after every command
	# This prevents a loss of history in the case of an unclean exit
	history -a;
	
	# Make the dollar sign bold if there are background jobs, and
	# underlined if there are stopped jobs
	local dollar_colour="";
	if [ "$(jobs -r | wc -l)" -gt "0" ]; then
		dollar_colour="${HC}";
	fi
	if [ "$(jobs -s | wc -l)" -gt "0" ]; then
		dollar_colour="${dollar_colour}${UL}";
	fi
	
	PS1="${debian_chroot:+($debian_chroot)}${HOST_COLOUR}\u$(prompt_at)\H\[\033[00m\]$(prompt_colon)\[\033[01;34m\]\w\[\033[00m\]";
	if [[ "$UID" = 0 ]]; then
		PS1="${PS1}${dollar_colour}#${RS} "
	else
		PS1="${PS1}${dollar_colour}\$${RS} "
	fi
	
	# If this is an xterm set the title to user@host:dir
	if [[ -z "${TITLE_SET_MANUALLY}" ]]; then
		case "$TERM" in
		xterm*|rxvt*)
			PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]${PS1}" ;;
		*) ;;
		esac
	fi
}

# https://help.ubuntu.com/community/CustomizingBashPrompt
# Was [01;32m;
if [ "$color_prompt" = yes ]; then
	#if [[ -w "$(pwd)" ]]; then
	#    PROMPT_COLON=$(echo -e "\033[32m:\033[0m")
	#else
	#    PROMPT_COLON=$(echo -e "\033[31m:\033[0m")
	#fi
	
	PROMPT_COMMAND=set_bash_prompt
else
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
	PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
	;;
*)
	;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	if test -r ~/.dircolors ; then
		eval "$(dircolors -b ~/.dircolors)";
	else
		eval "$(dircolors -b)";
	fi
	
	if ls /dev/shm --hyperlink=auto >/dev/null 2>&1; then
		alias ls='ls --color=auto --hyperlink=auto';
	else
		alias ls='ls --color=auto';
	fi
	alias dir='dir --color=auto'
	alias vdir='vdir --color=auto'
	
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Set up z.
# More information: https://github.com/rupa/z
#_Z_OWNER=sbrl
#. /home/sbrl/bin/z.sh

# Add pd
#shellcheck disable=SC1090 source=/dev/null
source "${HOME}/bin/pd-setup.sh"

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
	#shellcheck disable=SC1090 source=/dev/null
	source "${HOME}/.bash_aliases";
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
	. /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
  fi
fi

#shellcheck disable=SC1090
source ~/bin/commacd.sh

if [ "${NO_HH}" != "true" ] && [[ $- =~ .*i.* ]]; then
	# hh / hstr configuration (https://github.com/dvorka/hstr/)
	export HSTR_CONFIG=hicolor	# get more colors
	
	hstr_loc="";
	if which hh >/dev/null 2>&1; then
		hstr_loc="hh";
	elif which hstr >/dev/null 2>&1; then
		hstr_loc="hstr";
	fi
	
	if [[ -n "${hstr_loc}" ]]; then
		hstr_mode="$($hstr_loc --is-tiocsti)";
		
		if [[ "${hstr_mode}" == "n" ]]; then
			# erk, no tiocsti available.
			function hstrnotiocsti {
				{ READLINE_LINE="$( { </dev/tty ${hstr_loc} ${READLINE_LINE}; } 2>&1 1>&3 3>&- )"; } 3>&1;
				READLINE_POINT=${#READLINE_LINE}
			}
			# if this is interactive shell, then bind hstr to Ctrl-r (for Vi mode check doc)
			if [[ $- =~ .*i.* ]]; then bind -x '"\C-r": "hstrnotiocsti"'; fi
			export HSTR_TIOCSTI=n

		else
			# tiocsti is enabled, bind as normal
			bind '"\C-r": "\C-a '"${hstr_loc}"' \C-j"';
		fi
		
		unset hstr_mode
	fi
	
	unset hstr_loc
fi


if type "module" >/dev/null 2>&1; then
	echo "Viper HPC module loading system detected (module command present), not setting language and clearing existing language variables";
	unset LANG;
	unset LC_ALL;
	unset LANGUAGE;
else
	export LANG=en_GB.utf8;
	export LC_ALL="${LANG}";
	export LANGUAGE="${LANG}";
fi

# dh_make configuration
DEBEMAIL="feedback@starbeamrainbowlabs.com"
DEBFULLNAME="Starbeamrainbowlabs"
export DEBEMAIL DEBFULLNAME;

# micro true colour support
export MICRO_TRUECOLOR=1;

# less: Show colours, and enable unicode character support
export LESS="-R";
export LESSCHARSET=utf-8;

# ack: Specify .ackrc location
export ACKRC="${HOME}/bin/settings/.ackrc";

# tldr autocompletion
cachedir="${HOME}/.local/share/tldr" # Or whatever else the location of the tldr cache is
complete -W "$(q=(${cachedir}/*/*); sed 's@\.md @ @g' <<<"${q[@]##*/}")" tldr
unset cachedir;

# By this point, we've imported ~/.bash_aliases
# Automatically set the window title, but only if we're over ssh
if [ "${TERMINAL_TYPE}" = "ssh" ]; then
	if [[ -z "${HOSTNAME_PRETTY}" ]]; then
		set-title "$(hostname)";
	else
		set-title "${HOSTNAME_PRETTY}";
	fi
	GPG_TTY="$(tty)"; # Fix signing git commits with gpg over ssh, ref https://stackoverflow.com/a/49633685/1460422
	export GPG_TTY;
fi

# Add ~/bin & /sbin to PATH if they haven't already been added
if [[ ":$PATH:" != *":$HOME/bin:"* ]]; then
	export PATH=$PATH:$HOME/bin;
fi
if [[ ":$PATH:" != *":/sbin:"* ]]; then
	export PATH=$PATH:/sbin;
fi
if [[ -d "$HOME/.local/bin" ]] && [[ ":${PATH}:" != *":$HOME/.local/bin:"* ]]; then
	export PATH="$PATH:$HOME/.local/bin";
fi

# Make sure the HOSTNAME environment variable is always set
if [ -z "${HOSTNAME}" ]; then
	if [ -r "/etc/hostname" ]; then # Read from /etc/hostname if it exists
		HOSTNAME="$(cat /etc/hostname)";
	else # else call the hostname command
		HOSTNAME="$(hostname)";
	fi
fi

# SSH agent
# Adapted from https://stackoverflow.com/a/18915067/1460422
# Also adapted from https://stackoverflow.com/a/34332776/1460422
do_agent="no";
if [[ -n "${DO_SSH_AGENT}" ]]; then do_agent="yes"; fi
if [[ -n "${SSH_AGENT_ONLY_SSH}" ]] && [[ "${TERMINAL_TYPE}" != "ssh" ]]; then
	do_agent="no";
fi
if [[ "${do_agent}" == "yes" ]]; then
	SSH_ENV="${HOME}/.ssh/agent-environment";
	
	start_agent() {
	    echo -n "[bashrc:ssh_agent] Initialising new SSH agent... ";
	    /usr/bin/ssh-agent | sed 's/^echo/#echo/' >"${SSH_ENV}";
	    echo "succeeded";
	    chmod 600 "${SSH_ENV}";
		#shellcheck disable=SC1090 source=/dev/null
	    source "${SSH_ENV}" >/dev/null;
		if [[ -z "${SSH_AGENT_NONINTERACTIVE}" ]]; then
		    /usr/bin/env ssh-add;
		fi
	}
	
	# Source SSH settings, if applicable
	if [[ -r "${SSH_ENV}" ]]; then
		#shellcheck disable=SC1090 source=/dev/null
	    source "${SSH_ENV}" > /dev/null
	    #ps ${SSH_AGENT_PID} doesn't work under cywgin
	    
		if [[ ! -d "/proc/${SSH_AGENT_PID}" ]]; then
			echo "SSH agent PID ${SSH_AGENT_PID} appears to be dead";
			start_agent;
		else
			echo "Connected to existing SSH agent";
	    fi
	else
		echo "Starting new agent";
	    start_agent;
	fi
	new_sock="${HOME}/.ssh/ssh_agent.$$.$USER.sock";
	ln -Ts "${SSH_AUTH_SOCK}" "${new_sock}";
	export SSH_AUTH_SOCK="${new_sock}";
	
	end_agent() {
		nhard="$(find "${SSH_AUTH_SOCK}" -mindepth 1 -maxdepth 1 | awk '{ print $2 }')";
		if [[ "${nhard}" -eq 2 ]]; then
			echo "Killing ssh-agent";
			rm "${SSH_ENV}";
			ssh-agent -k >/dev/null;
		fi
		rm "${SSH_AUTH_SOCK}";
	}
	trap end_agent EXIT;
	
	unset new_sock;
	unset -f start_agent;
fi
unset do_agent;


# Rust configuration
export RUSTUP_HOME="$HOME/.config/rustup";
export CARGO_HOME="$HOME/.config/cargo";
export PATH="$PATH:$CARGO_HOME/bin";
if which sccache >/dev/null 2>&1; then
	export RUSTC_WRAPPER=sccache;
fi

export UNCRUSTIFY_CONFIG="$HOME/bin/settings/uncrustify.cfg";

# Why systemd? Just why? It's really annoying....
export SYSTEMD_PAGER=cat


# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth;

# HSTR: Apply patch if on Linux 6.2+
if [[ "$(echo "$(uname -r | cut -c1-3) > 6.2" | bc -lq)" ]]; then
	hstrnotiocsti() {
		#shellcheck disable=SC2086
		{ READLINE_LINE="$( { </dev/tty hstr ${READLINE_LINE}; } 2>&1 1>&3 3>&- )"; } 3>&1;
		READLINE_POINT=${#READLINE_LINE}
	}
	# if this is interactive shell, then bind hstr to Ctrl-r (for Vi mode check doc)
	if [[ $- =~ .*i.* ]]; then bind -x '"\C-r": "hstrnotiocsti"'; fi
	export HSTR_TIOCSTI=n;
fi

if command -v zoxide >/dev/null 2>&1; then
	eval "$(zoxide init bash)";
elif [[ ! -e "${HOME}/.config/__zoxide_noinstalled_shown.txt" ]]; then
	echo ">> zoxide is not installed. This message won't be shown again." >&2;
	touch "${HOME}/.config/__zoxide_noinstalled_shown.txt";
fi
